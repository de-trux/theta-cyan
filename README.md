# detrux's Theta Cyan dotfiles

![image info](./preview/01.png)
![image info](./preview/02.png)

## Programs Used
* `bspwm`, Window Manager
* `picom`, [compositor](https://github.com/yshui/picom)
* `feh`, image viewer / background setter
* `polybar`, heavily modified [adi1090x](https://github.com/adi1090x/polybar-themes) bar (colorblocks)
* `zsh` + `starship` + `zsh-syntax-highlighting` + `zsh-autosuggestions`, shell and prompt
* `dunst`, notification daemon
* `rofi`, application launcher and menu
* `mpd`+`mpc`+`ncmpcpp`, music player and daemon
* `networkmanager-dmenu`, network menu
* `brillo`, [brightness control](https://www.youtube.com/watch?v=pGOaSS8nEQA)
* `kitty` , terminal
* `conky`, [system monitor](https://www.gnome-look.org/p/1856062)
* `pfetch`+`neofetch`, system info fetch
* `pulseaudio`+`pipewire` audio things
* `pywal`, `scrot`, `thunar`, `firefox`, `discord-canary`, `vim`, `gedit`, `qt5ct`, `lxappearance`, `xfce4-settings`, etc..

## Details

My current setup works best if you want 125% scaling factor on HiDPI display, just make sure to remove/comment these things if you want normal 100% scaling

btw my current display is 1920x1080 on 12,5" eDP1 monitor

```
~/.Xresources
-----------------
Xft.dpi: 120
```
and
```
~/.config/polybar/config.ini
-----------------
dpi = 120
```

after that, you probably need to manually reconfigure the font size for each programs by yourself
### Fonts
* Fantasque Sans Mono
* Nerd Font Symbols
* Carlito

### Themes
* **GTK2/3** [Matcha Dark](https://github.com/vinceliuice/Matcha-gtk-theme) (Sea)
* **QT** Dark Breeze

### Icons
* Papirus icon dark variant

### Wallpaper
* [idk](https://www.wallpaperflare.com/anime-hat-anime-girls-tea-closed-eyes-simple-background-wallpaper-cpigv)

### Notes
udev rules in the `etc` file is for enable the control of backlight from regular user (needed by brillo), otherwise regular user won't have permission to control backlight, in this case i use [intel backlight](https://wiki.archlinux.org/title/Backlight#ACPI)

then you need to add $USER into `video` group : `usermod -aG $USER video`

